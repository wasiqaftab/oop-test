<?php 

class TopSolilBag {

    public $measurement_unit  = '' ;
    public $depth_unit  = '' ; 
    private $conn = null ;
    function __construct($m_unit = "meters", $d_unit = "inches")
    {
        $this->measurement_unit = $m_unit ; 
        $this->depth_unit = $d_unit ;

        $servername = "localhost";
        $username = "root";
        $password = "";
        $this->conn = new mysqli($servername, $username, $password, 'tapsoil_db');
    }

    function check_units(){
        return "<b>Measurement Unit</b> : </b>".$this->measurement_unit. "</br> <b>Depth Unit :</b>".$this->depth_unit ;
    }
    function depth_calc($depth)
    {
        $depth_value = null ;
        if($this->depth_unit === "inches")
        {
            $depth_cal = $depth / 39.37 ;
            $depth_value = number_format((float)$depth_cal, 3, '.', '') ;
        }
        else if($this->depth_unit === "centimetres")
        {
            $depth_cal = $depth * 0.01 ;
            $depth_value = number_format((float)$depth_cal, 3, '.', '') ;
        }

        return $depth_value ;
    }
   
    function calculate_area($width, $height, $depth)
    {
        $calc_vol =  null ;
        $depth_val = $this->depth_calc($depth) ;

        if($this->measurement_unit === "feets"){
            $calc_vol = (($width / 3.28) * ($height/ 3.28)) * $depth_val ;
        }
        else if($this->measurement_unit === "yards"){
            $calc_vol = (($width* 0.9144) * ($height* 0.9144)) * $depth_val  ;
        }
        else{
            $calc_vol = $width * $height * $depth_val ;
        }
            return $calc_vol ;
    }

    function bag_calculation($w, $h, $d)
    {
        $volume = $this->calculate_area($w, $h, $d) ;
        $x = $volume * 0.025 ;
        $y = $x * 1.4 ;
        $bags = ceil(number_format((float)$y, 3, '.', '')) ;
        return $bags ;
    }

    function store_tapsoil($m_unit, $d_unit,$bags, $price){
        $store_tapsoil = "INSERT INTO tapsoil(measurement_unit, depth_unit, total_bags, price) 
                    VALUES('$m_unit','$d_unit','$bags','$price')" ;

        if(!empty($store_tapsoil)) {
            $result = mysqli_query($this->conn, $store_tapsoil);
            if($result) {
                return "</br> Records Successfully Saved!" ;
            }
            else
            {
                return  "</br> Records Failed to INSERT!" ;
            }
        }
    }
}

$obj = new TopSolilBag("feets","centimetres") ;
// $total_bags =  ($obj->bag_calculation(9,5,2.45)) ;

//depth in inches, width and height in meters
//$total_bags =  ($obj->bag_calculation(9,5,96.45)) ;

//depth in inches, width and height in yards
//$total_bags =  ($obj->bag_calculation(9.84,5.46,96.45)) ;

//depth in inches, width and height in feets
//$total_bags =  ($obj->bag_calculation(29.52,16.40,96.45)) ;


//depth in centimeters, width and height in meters
//$total_bags =  ($obj->bag_calculation(9,5,245)) ;

//depth in centimeters, width and height in yards
//$total_bags =  ($obj->bag_calculation(9.84,5.46,245)) ;

//depth in centimeters, width and height in feets
$total_bags =  ($obj->bag_calculation(29.52,16.40,245)) ;


echo  $obj->check_units()."</br>" ;
echo "<b>Total Number of Bags :</b> ". $total_bags ;
$total_price = $total_bags * 72 ;
echo "</br> <b> Topsoil bag cost: £72 (inc VAT) : </b>".$total_price ;

echo $obj->store_tapsoil($obj->measurement_unit, $obj->depth_unit,$total_bags,$total_price) ;


?>